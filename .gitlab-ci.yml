# Default image: the image used to create job containers when it is not
# redefined in the job declaration.
# In that case, the official Maven image v3.6 with the JDK 8
# See https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#what-is-an-image
# See https://docs.gitlab.com/ee/ci/yaml/README.html#setting-default-parameters
image: maven:3.6-jdk-8-slim

# Gloabal environment variables that are available within each job container environment
# See https://docs.gitlab.com/ee/ci/yaml/README.html#variables
# See https://docs.gitlab.com/ee/ci/variables/README.html#gitlab-cicd-environment-variables
variables:
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
  CONTAINER_IMAGE: $CI_REGISTRY_IMAGE
  MAVEN_CLI_OPTS: "--strict-checksums --threads 1C --batch-mode"
  # Maven option to use a local Maven repository for cache purposes
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"

# Global cache paths. They can be redefined in the job declaration
# See https://docs.gitlab.com/ee/ci/yaml/README.html#cache
cache:
  key: "$CI_COMMIT_REF_SLUG"
  paths:
    - .m2/repository/ # Local Maven repository
    - target/ # Defalt output folder of Maven artifacts

# Stages definition
# See https://docs.gitlab.com/ee/ci/yaml/README.html#stages
stages:
  - build
  - verify
  - unit-test
  - integration-test
  - package
  - release
  - docs
  - deploy

# Build job that compile the Spring Boot app with Maven
# See introduction to jobs https://docs.gitlab.com/ee/ci/yaml/README.html#introduction
compile:
  stage: build
  # The actual commands to be executed
  # See https://docs.gitlab.com/ee/ci/yaml/README.html#script
  script:
    - mvn $MAVEN_CLI_OPTS $MAVEN_OPTS compile

# Lint job that perform style checks on the source code
lint:
  stage: verify
  # Redefine cache overriding the key in order to create a dedicated cache for
  # this job. This Maven goal downloads additional dependencies to perform the
  # style checks
  cache:
    key: "$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG"
    paths:
      - .m2/repository/
      - target/
  script:
    - mvn $MAVEN_CLI_OPTS $MAVEN_OPTS checkstyle:check
  # This job will fail and we are allowing it to fail for teaching purposes
  allow_failure: true
  # Store artifacts when the job fails. Artifacts can be downloaded later or used
  # by the next jobs
  # See https://docs.gitlab.com/ee/ci/yaml/README.html#artifacts
  # See https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html
  artifacts:
    paths:
      - target/checkstyle-result.xml
    when: on_failure #See https://docs.gitlab.com/ee/ci/yaml/README.html#artifactswhen

# Static Analysis job that uses SpotBugs
sats:
  # Note the same stage of the previous job. These two verify jobs will run in parallel
  stage: verify
  cache:
    key: "$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG"
    paths:
      - .m2/repository/
      - target/
  script:
    - mvn $MAVEN_CLI_OPTS $MAVEN_OPTS compile spotbugs:check
  # Store SpotBugs results if the the job fails
  artifacts:
    paths:
      - target/spotbugsXml.xml
    when: on_failure
  # Explicit declaration of 0 dependencies to disable previous job's artifacts
  # download
  # See https://docs.gitlab.com/ee/ci/yaml/#dependencies
  dependencies: []

# Unit tests job
unit-test:
  stage: unit-test
  # Attach network services to the main job container, in this case MongoDb 3.7
  # See https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#what-is-a-service
  services:
  - name: mongo:3.7
    alias: mongodb
  cache:
    key: "$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG"
    paths:
      - .m2/repository/
  script:
    - mvn $MAVEN_CLI_OPTS $MAVEN_OPTS test
    # Print the code coverage results to the stdout to enable code coverage regex
    # See https://docs.gitlab.com/ee/user/project/pipelines/settings.html#test-coverage-parsing
    - cat target/coverage/index.html
  dependencies: []

# Integraton test job
integration-test:
  stage: integration-test
  services:
  - name: mongo:3.7
    alias: mongodb
  cache:
    key: "$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG"
    paths:
      - .m2/repository/
  script:
    - mvn $MAVEN_CLI_OPTS $MAVEN_OPTS -DskipUTs verify
  dependencies: []
  # Run this job only on the master branch pipeline
  only:
  - master

# Package the Spring Boot app job, it will create the .jar file
springboot-app:
  stage: package
  cache:
    key: "$CI_COMMIT_REF_SLUG"
    paths:
      - .m2/repository/
    # Just pull the cache, it will not be updated
    # See https://docs.gitlab.com/ee/ci/yaml/README.html#cachepolicy
    policy: pull
  script:
    - mvn $MAVEN_CLI_OPTS $MAVEN_OPTS -DskipTests clean package spring-boot:repackage
  dependencies: []
  artifacts:
    paths:
      - target/*.jar
    when: on_success

# Build the Docker image of the Spring Boot app and push it to the GitLab
# Container Registry.
# It will use the Dockerfile in the repository's root
# See https://docs.gitlab.com/ee/ci/docker/using_docker_build.html
docker-image:
  stage: release
  # Redefine the job container image
  image: docker:stable
  # Use Docker-in-Docker service
  # See https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-workflow-with-docker-executor
  services:
    - docker:dind
  cache: {} # Disable cache
  # Explicit declation of job dependencies to download only their artifacts
  dependencies:
    - springboot-app
  script:
    # Login into GitLab Container Registry
    # See https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#authenticating-to-the-container-registry
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    # Optimizing Docker caching
    # See https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#using-docker-caching
    - docker pull $CONTAINER_IMAGE:$CI_COMMIT_REF_NAME || true
    - docker build --cache-from $CONTAINER_IMAGE:$CI_COMMIT_REF_NAME -t $CONTAINER_IMAGE:$CI_COMMIT_REF_NAME .
    - docker push $CONTAINER_IMAGE:$CI_COMMIT_REF_NAME

# Generate Javadoc and publish as GitLab Pages
# See https://docs.gitlab.com/ee/user/project/pages/introduction.html
pages:
  stage: docs
  cache:
    key: "$CI_COMMIT_REF_SLUG"
    paths:
      - .m2/repository/
    policy: pull
  dependencies: []
  script:
    - mvn $MAVEN_CLI_OPTS $MAVEN_OPTS javadoc:javadoc
    - mv target/site/apidocs public
  artifacts:
    paths:
      # The folder that contains the files to be exposed at the Page URL (https://sw_dev_process_course.gitlab.io/my-dummy-pipeline/)
      - public
    when: on_success
  only:
    - master

# Deploy the Spring Boot app with SSH exploiting the Docker image just pushed
deploy-staging:
  stage: deploy
  image: debian:stable-slim
  # Run this script before the actual deploy script
  # See https://docs.gitlab.com/ee/ci/yaml/README.html#before_script-and-after_script
  before_script:
    - chmod 400 $SSH_PRIVATE_KEY
    - 'which ssh-agent || ( apt-get update -qq && apt-get install -qq openssh-client )'
    - eval $(ssh-agent -s)
    - ssh-add $SSH_PRIVATE_KEY
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  cache: {}
  dependencies: []
  # Link to the deployment environment
  # See https://docs.gitlab.com/ee/ci/environments.html
  environment:
    name: staging
    url: http://$STAGING_IP
  only:
    - master
  when: manual
  script:
    - chmod +x .gitlab-ci-deploy-staging.sh
    - bash .gitlab-ci-deploy-staging.sh # See this file for further details